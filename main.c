#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>

#define MAPSIZE_X 15 // side to side
#define MAPSIZE_Y 15 // up and down
#define PLAYERS 2 // amount of "ai's" or players
#define TASKS 3

char map[MAPSIZE_Y+2][MAPSIZE_X+2];

void print_map() {
    for (int i = 0; i < MAPSIZE_Y+2; i++){
        for(int j = 0; j < MAPSIZE_X+2; j++) {
            printf("%c", map[i][j]);
        }
        printf("\n");
    }
}

void generate_map() {

    /* Generate map */
    for (int i = 0; i < MAPSIZE_Y+2; i++){
        for(int j = 0; j < MAPSIZE_X+2; j++) {
            map[i][j] = '-';
        }
    }
    /* Generate map top border */
    for (int i = 0; i < MAPSIZE_X+2; i++) {
        map[0][i-1] = '#';
        map[0][MAPSIZE_X+1] = '#';
    }
    /* Generate map bottom border */
    for (int i = 0; i < MAPSIZE_X+2; i++) {
        map[MAPSIZE_Y+1][i] = '#';
    }
    /* Generate map sides */
    for (int i = 0; i < MAPSIZE_Y+2; i++) {
        map[i][0] = '#';
    }
    for (int i = 0; i < MAPSIZE_Y+2; i++) {
        map[i][MAPSIZE_X+1] = '#';
    }
}

int main() {
    int player_x, player_y;
    /* In the AI var, index 0 does not exist.
       If you wanted to access ai #1, 
       you would access ai_x[1] or ai_y[1] */
    int ai_x[PLAYERS], ai_y[PLAYERS];

    generate_map();
    /* Put the player in the middle of the map */
    player_x = MAPSIZE_X / 2 + 1;
    player_y = MAPSIZE_Y / 2 + 1;
    map[player_y][player_x] = 'o';

    /* Put the AI's in random places */
    for (int i = 0; i < PLAYERS; i++) {
	ai_x[i] = player_x;
	ai_y[i] = player_y;
	while (map[ai_y[i]][ai_x[i]] != '#') {
	    srand(time(NULL));
            ai_x[i] = (rand() % (MAPSIZE_X - 2 + 1)) + 2;
            ai_y[i] = (rand() % (MAPSIZE_Y - 2 +1)) + 2;
        }
	map[ai_y[i]][ai_x[i]] = 'a';
    }
    print_map();

}

