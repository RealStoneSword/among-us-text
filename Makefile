COMPILER = gcc
FLAGS = -Werror -Wall -Wextra -pedantic-errors -std=c99 -Wno-unused-variable -Wno-unused-but-set-variable

TARGET = a.out
FILENAME = main.c

all: 
	$(COMPILER) $(FLAGS) -o $(TARGET) $(FILENAME)

clean:
	rm -rf $(TARGET)
	$(COMPILER) $(FLAGS) -o $(TARGET) $(FILENAME)
